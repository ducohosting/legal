![Duco Hosting Logo](/logo.png)
#Terms of service

> This file is no longer updated as of October 14th 2016.. The most recent changes can be found [here](https://github.com/ducohosting/legal/commits/master/terms.adoc)

> This is the new terms file as of August 20th 2014 at 8PM GMT+1. Old term changes can be found [here](https://github.com/ducohosting/legal/commits/master/terms.html)

##1. Acceptance of Terms

This is an agreement between Duco Hosting (we, us, Duco Hosting) and Duco Hosting customers (you, clients, customers, users)
Duco Hosting provides its service to customers subject to the following Terms of Service ("TOS"), which may be updated by us from time to time without notice to you. In addition, when using particular Duco Hosting owned or operated services, you and Duco Hosting shall be subject to any posted guidelines or rules applicable to such services, which may be posted from time to time. All such guidelines or rules (including but not limited to our Spam Policy) are hereby incorporated by reference into the TOS. Duco Hosting may also offer other services that are governed by different Terms of Service.

##2. Description of Service

Duco Hosting provides users with access to a rich collection of resources, including various communications tools, forums, shopping services, search services, personalized content and branded programming through its network of properties which may be accessed through any various medium or device now known or hereafter developed (the "Service"). You also understand and agree that the Service may include advertisements and that these advertisements are necessary for Duco Hosting to provide the Service. You also understand and agree that the Service may include certain communications from Duco Hosting , such as service announcements, administrative messages and the Duco Hosting Newsletter, and that these communications are considered part of Duco Hosting membership and you will not be able to opt out of receiving them. Unless explicitly stated otherwise, any new features that augment or enhance the current Service, including the release of new Duco Hosting properties, shall be subject to the TOS. You understand and agree that the Service is provided "AS-IS" and that Duco Hosting assumes no responsibility for the timeliness, deletion, mis-delivery or failure to store any user communications or personalization settings. You are responsible for obtaining access to the Service, and that access may involve third-party fees (such as Internet service provider or airtime charges). You are responsible for those fees, including those fees associated with the display or delivery of advertisements. In addition, you must provide and are responsible for all equipment necessary to access the Service.

##2.1 Technical Support

Customers with a paid Hosting Account have the ability to contact Duco Hosting directly for technical support. Subject to the TOS, Duco Hosting shall use commercially reasonable efforts to provide technical support to its customers. Duco Hosting shall provide technical support solely during the term of the service. Customers with a paid Hosting Account shall be entitled to submit a commercially reasonable number of technical support requests as determined by Duco Hosting in its sole discretion. The excessive and unreasonable submission of technical support requests (as determined by Duco Hosting in its sole discretion) by customers shall constitute unacceptable use and give rise to Duco Hosting's termination and suspension rights under these TOS. In the event a Customer's account is not in good standing then the Customer may be denied support until the account issues have been resolved.

##3. Your registration obligations

In consideration of your use of the Service, you represent that you are of legal age to form a binding contract and are not a person barred from receiving services under the laws of the Netherlands or other applicable jurisdiction. You also agree to:
1. provide true, accurate, current and complete information about yourself as prompted by the Service's registration form (the "Registration Data") and
2. maintain and promptly update the Registration Data to keep it true, accurate, current and complete. If you provide any information that is untrue, inaccurate, not current or incomplete, or Duco Hosting has reasonable grounds to suspect that such information is untrue, inaccurate, not current or incomplete, Duco Hosting has the right to suspend or terminate your account and refuse any and all current or future use of the Service (or any portion thereof).

Duco Hosting is concerned about the safety and privacy of all its users, particularly children. Please remember that the Service is designed to appeal to a broad audience. Accordingly, as the legal guardian, it is your responsibility to determine whether any of the Service areas and/or Content (as defined in Section 6 below) are appropriate for your child.


##3.1. Domain registrations

All domain registrations, renewals or transfers processed by Duco Hosting are subject to the [eNom Domain Name Registration Agreement](http://en.wikipedia.org/wiki/Inode).

Domains shall be renewed at the same price as new registrations. The current pricing is displayed on our website.

Domain renewal reminders will be sent in advance of expiration via email to the address stored in your client area.

Should an expired domain enter the redemption period, the cost of restoring the domain shall be no more than $300 USD.

##4. Duco Hosting Privacy Policy

Registration Data and certain other information about you is subject to our Privacy Policy. For more information, see our full Privacy Policy, You understand that through your use of the Service you consent to the collection and use (as set forth in the Privacy Policy) of this information, including the transfer of this information to the United States and/or other countries for storage, processing and use by Duco Hosting and its affiliates.

##5. Member account, password and security

You will receive a password and account designation upon completing the Service's registration process. You are responsible for maintaining the confidentiality of the password and account and are fully responsible for all activities that occur under your password or account. You agree to
1. immediately notify Duco Hosting of any unauthorized use of your password or account or any other breach of security, and
2. ensure that you exit from your account at the end of each session. Duco Hosting cannot and will not be liable for any loss or damage arising from your failure to comply with this Section 5.

##6. Member Conduct

You understand that all information, data, text, software, music, sound, photographs, graphics, video, messages, tags, or other materials ("Content"), whether publicly posted or privately transmitted, are the sole responsibility of the person from whom such Content originated. This means that you, and not Duco Hosting , are entirely responsible for all Content that you upload, post, email, transmit or otherwise make available via the Service. Duco Hosting does not control the Content posted via the Service and, as such, does not guarantee the accuracy, integrity or quality of such Content. You understand that by using the Service, you may be exposed to Content that is offensive, pornographic, indecent or objectionable. Under no circumstances will Duco Hosting be liable in any way for any Content, including, but not limited to, any errors or omissions in any Content, or any loss or damage of any kind incurred as a result of the use of any Content posted, emailed, transmitted or otherwise made available via the Service.

You agree to not use the Service to:

* upload, post, email, transmit or otherwise make available any Content that is unlawful, harmful, threatening, abusive, harassing, tortious, defamatory, vulgar, obscene, libelous, invasive of another's privacy, hateful, or racially, ethnically or otherwise objectionable;
* harm minors in any way;
* impersonate any person or entity, including, but not limited to, a Duco Hosting official, forum leader, guide or host, or falsely state or otherwise misrepresent your affiliation with a person or entity;
* forge headers or otherwise manipulate identifiers in order to disguise the origin of any Content transmitted through the Service;
* upload, post, email, transmit or otherwise make available any Content that you do not have a right to make available under any law or under contractual or fiduciary relationships (such as inside information, proprietary and confidential information learned or disclosed as part of employment relationships or under nondisclosure agreements);
* upload, post, email, transmit or otherwise make available any Content that infringes any patent, trademark, trade secret, copyright or other proprietary rights ("Rights") of any party; This includes linking to or redirecting to any content or copyright files hosted on a 3rd party resource / servers.
* upload, post, email, transmit or otherwise make available any unsolicited or unauthorized advertising, promotional materials, "junk mail," "spam," "chain letters," "pyramid schemes," or any other form of solicitation, except in those areas (such as shopping) that are designated for such purpose (please read our complete Spam Policy);
* upload, post, email, transmit or otherwise make available any material that contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment;
* upload, post, email, transmit or otherwise make available any material that is of broadcast / streaming types.
* upload, post, email, transmit or otherwise make available any material that is of
* keylogging / proxy service / irc / shell(s) of any type / file hosting / file sharing types.
* upload, post, email, transmit or otherwise make available any material on personal hosting accounts that is of pornographic nature. This excludes business hosting accounts.
* interfere with or disrupt the Service or servers or networks connected to the Service, or disobey any requirements, procedures, policies or regulations of networks connected to the Service;
* "stalk" or otherwise harass another; and/or
* upload, post, email, transmit or otherwise material for the purposes of file distribution, relay, or streaming reasons.
* collect or store personal data about other users in connection with the prohibited conduct and activities set forth in paragraphs 1 through 15 above.

##7. Use of copyrighted material and proof of ownership of content

Sites must not contain Warez, copyrighted material or other illegal material including links or redirects to copyrighted material hosted on 3rd party websites / resources. The onus is on you the customer to prove that you own the rights to publish material, not for Duco Hosting to prove that you do not.  Duco Hosting does not allow the propagation or distribution of copyright material, files or warez under any circumstances.

##8. Acceptable resource use

Sites must not use excessive amounts of server resources. These include bandwidth, processor utilization and / or disk space. Please see the 'High Resource Use Policy' in the General Terms and Conditions.
Bandwidth usage is calculated and updated once every 24 hours, Duco Hosting may not be held accountable for overage fees due to delayed resource limitations.

##9. Script usage Terms

Scripts on the site must be designed to produce web-based content, and not to use the server as an application server. Using the server to generate large volumes of email from a database is an example of activity that is not allowed. Scripts should not attempt to manipulate the timeouts on servers. These are set at the present values to ensure the reliability of the server. Sites that reset these do so because they are resource intensive, and adversely affect server performance and are therefore not allowed. Scripts that are designed to provide proxy services, anonymous or otherwise, are not allowed.

The primary purpose of any script must be to produce a web page. Scripts that send a single email based upon user entered information, or update a database are acceptable. Scripts that send bulk email or perform processor intensive database processes are not allowed. All outgoing mail is monitored and filtered and must be sent to or from a Duco Hosting -hosted domain.

Sites must not contain scripts that attempt to access privileged server resources, or other sites on the same server

**USAGE OF DISK SPACE TERMS**

Duco Hosting offers large web space and bandwidth with hosting accounts. By this, we mean space for legitimate web site content and bandwidth for visitors to view it. All files on a domain must be part of the active website and linked to the site. Sites should not contain any backups (All accounts on the server are automatically backed up by us daily), downloads, or other non-web based content. We will treat all password protected archive (e.g. zip and rar) files as unacceptable. Multimedia content such as audio and video is acceptable provided it is streamed to the user, links to HTTP download of this content is not acceptable.

On unlimited disk space plans we offer quotaless disk space access, however the disks in servers are not unlimited so you may become limited if a hard drive begins to fill to capacity

##10. inode usage

In computing, an inode is a data structure on a traditional Unix-style file system such as UFS. An inode stores basic information about a regular file, directory, or other file system object.
[Wikipedia article on inodes](http://en.wikipedia.org/wiki/Inode)

Duco Hosting reserves the right to limit INODEs usage on free hosting accounts to 768 INODEs (With a five-day grace period up to 1024) , premium hosting plans to 180,000 (With a five-day grace period up to 200,000) and Business hosting plans to unlimited subject to acceptable usage.

When a hosting account reaches the grace period limit, or stays above the regular limit for more than five days. No more data can be written to the disk until the amount of used INODEs is decreased.

##11. Right to suspend/terminate hosting services

We reserve the right to suspend or terminate any hosting services if a website/hosting service consumes excessive server resources or effects other websites / hosting services on a shared or clustered hosting server. Excessive resource usage can constitute any form of server usage calculated and determined at the discretion of Duco Hosting . Duco Hosting reserves the right to suspend service at any time without prior notice if or when the health or normal running / performance of a service is effected / degraded by a website or hosting service.

You acknowledge that Duco Hosting may or may not pre-screen Content, but that Duco Hosting and its designees shall have the right (but not the obligation) in their sole discretion to pre-screen, refuse, or remove any Content that is available via the Service. Without limiting the foregoing, Duco Hosting and its designees shall have the right to remove any Content that violates the TOS or is otherwise objectionable. You agree that you must evaluate, and bear all risks associated with, the use of any Content, including any reliance on the accuracy, completeness, or usefulness of such Content. In this regard, you acknowledge that you may not rely on any Content created by Duco Hosting or submitted to Duco Hosting , including without limitation information in Duco Hosting Message Boards and in all other parts of the Service.

You acknowledge, consent and agree that Duco Hosting may access, preserve and disclose your account information and Content if required to do so by law or in a good faith belief that such access preservation or disclosure is reasonably necessary to:

1. comply with legal process;
2. enforce the TOS;
3. respond to claims that any Content violates the rights of third parties;
4. respond to your requests for customer service; or 5. protect the rights, property or personal safety of Duco Hosting , its users and the public.

You understand that the technical processing and transmission of the Service, including your Content, may involve
1. transmissions over various networks; and
2. changes to conform and adapt to technical requirements of connecting networks or devices.
You understand that the Service and software embodied within the Service may include security components that permit digital materials to be protected, and that use of these materials is subject to usage rules set by Duco Hosting and/or content providers who provide content to the Service. You may not attempt to override or circumvent any of the usage rules embedded into the Service. Any unauthorized reproduction, publication, further distribution or public exhibition of the materials provided on the Service, in whole or in part, is strictly prohibited.

##12. Invoice payment terms

You acknowledge, consent and agree that Duco Hosting may suspend a service if the associate invoice has not been paid before or on the due date.

You agree to pay a late fee of (i) 10% of the invoice amount; or (ii) €2.50 when 10% of the invoice amount is less than €2.50; when an invoice remains overdue for seven or more days.

You agree that Duco Hosting may forward your details to a debt collection agency when an invoice is more than thirty days overdue.

##13. Special admonitions for international use

Recognizing the global nature of the Internet, you agree to comply with all local rules regarding online conduct and acceptable Content. Specifically, you agree to comply with all applicable laws regarding the transmission of technical data exported from the United States or the country in which you reside.

##14. Content submitted or made available for inclusion on the service

Duco Hosting does not claim ownership of Content you submit or make available for inclusion on the Service. However, with respect to Content you submit or make available for inclusion on publicly accessible areas of the Service, you grant Duco Hosting the following worldwide, royalty-free and non-exclusive license(s), as applicable:

With respect to Content you submit or make available for inclusion on publicly accessible areas of Duco Hosting Servers, the license to use, distribute, reproduce, modify, adapt, publicly perform and publicly display such Content on the Service solely for the purposes of providing and promoting the Duco Hosting Service to which such Content was submitted or made available. This license exists only for as long as you elect to continue to include such Content on the Service and will terminate at the time you remove or Duco Hosting removes such Content from the Service.

With respect to photos, graphics, audio or video you submit or make available for inclusion on publicly accessible areas of the Service other than Duco Hosting Servers, the license to use, distribute, reproduce, modify, adapt, publicly perform and publicly display such Content on the Service solely for the purpose for which such Content was submitted or made available. This license exists only for as long as you elect to continue to include such Content on the Service and will terminate at the time you remove or Duco Hosting removes such Content from the Service.

With respect to Content other than photos, graphics, audio or video you submit or make available for inclusion on publicly accessible areas of the Service other than Duco Hosting Servers, the perpetual, irrevocable and fully sublicensable license to use, distribute, reproduce, modify, adapt, publish, translate, publicly perform and publicly display such Content (in whole or in part) and to incorporate such Content into other works in any format or medium now known or later developed.

"Publicly accessible" areas of the Service are those areas of the Duco Hosting network of properties that are intended by Duco Hosting to be available to the general public. By way of example, publicly accessible areas of the Service would include Duco Hosting Message Boards and all areas that are open to both members and visitors. However, publicly accessible areas of the Service would not include portions of Duco Hosting Servers that are limited to members, Duco Hosting services intended for private communication such as Duco Hosting Mail or, or areas off of the Duco Hosting network of properties such as portions of World Wide Web sites that are accessible via hypertext or other links but are not hosted or served by Duco Hosting .

##15. Contributions to Duco Hosting

By submitting ideas, suggestions, documents, and/or proposals ("Contributions") to Duco Hosting through its forum, or contact forums, you acknowledge and agree that:

1. your Contributions do not contain confidential or proprietary information;
2. Duco Hosting is not under any obligation of confidentiality, express or implied, with respect to the Contributions;
3. Duco Hosting shall be entitled to use or disclose (or choose not to use or disclose) such Contributions for any purpose, in any way, in any media worldwide;
4. Duco Hosting may have something similar to the Contributions already under consideration or in development;
5. your Contributions automatically become the property of Duco Hosting without any obligation of Duco Hosting to you; and
6. you are not entitled to any compensation or reimbursement of any kind from Duco Hosting under any circumstances.

##16. Indemnity

You agree to indemnify and hold Duco Hosting and its subsidiaries, affiliates, officers, agents, employees, partners and licensors harmless from any claim or demand, including reasonable attorneys' fees, made by any third party due to or arising out of Content you submit, post, transmit or otherwise make available through the Service, your use of the Service, your connection to the Service, your violation of the TOS, or your violation of any rights of another.

##17. No resale of service

You agree not to reproduce, duplicate, copy, sell, trade, resell or exploit for any commercial purposes, any portion of the Service (including your Duco Hosting ID), use of the Service, or access to the Service, unless you are a member of the Duco Hosting Reseller Service, utilising the reselling platform to resell to end users.

##18. General practices regarding use and storage

You acknowledge that Duco Hosting may establish general practices and limits concerning use of the Service, including without limitation the maximum number of days that idle hosting accounts, message board postings or other uploaded Content will be retained by the Service, the maximum number of email messages that may be sent from or received by an account on the Service, the maximum size of any email message that may be sent from or received by an account on the Service, the maximum disk space that will be allotted on Duco Hosting 's servers on your behalf, and the maximum number of times (and the maximum duration for which) you may access the Service in a given period of time. You agree that Duco Hosting has no responsibility or liability for the deletion or failure to store any messages and other communications or other Content maintained or transmitted by the Service. You acknowledge that Duco Hosting reserves the right to log off accounts that are inactive for an extended period of time. You further acknowledge that Duco Hosting reserves the right to modify these general practices and limits from time to time.

###Backup retention

Duco Hosting makes automatic backups of all hosting accounts every 24 hours. These backups are retained for 100 days before being deleted. Backups can be restored free of charge within the first seven days after their creation. You agree to pay a backup restoration fee when restoring a backup older than seven days. Duco Hosting will inform you about this fee before continuing with the restoration process.

##19. Modifications to service

Duco Hosting reserves the right at any time and from time to time to modify or discontinue, temporarily or permanently, the Service (or any part thereof) with or without notice. You agree that Duco Hosting shall not be liable to you or to any third party for any modification, suspension or discontinuance of the Service.

##20. Terminations

You agree that Duco Hosting may, under certain circumstances and without prior notice, immediately terminate your Duco Hosting account, any associated email address, and access to the Service. Cause for such termination shall include, but not be limited to,

1. breaches or violations of the TOS or other incorporated agreements or guidelines,
2. requests by law enforcement or other government agencies,
3. a request by you (self-initiated account deletions),
4. discontinuance or material modification to the Service (or any part thereof),
5. unexpected technical or security issues or problems,
6. extended periods of inactivity,
7. engagement by you in fraudulent or illegal activities, and/or
8. nonpayment of any fees owed by you in connection with the Services.

Termination of your Duco Hosting account includes

1. removal of access to all offerings within the Service, including but not limited to Hosting accounts, Email Services, SQL databases,
2. deletion of your password and all related information, files and content associated with or inside your account (or any part thereof), and
3. barring of further use of the Service. Further, you agree that all terminations for cause shall be made in Duco Hosting's sole discretion and that Duco Hosting shall not be liable to you or any third party for any termination of your account, any associated email address, or access to the Service.

##21. Refund policy

Services are eligible for refunds within 7 twenty-four hour days of the transaction made to activate the service. Refunds cannot be requested without probable cause; In addition to this non-monthly
terms are non-refundable.
Refunds will not be granted when a service is terminated in accordance with the terms set forth on this page.

##22. Dealings with advertisers

Your correspondence or business dealings with, or participation in promotions of, advertisers found on or through the Service, including payment and delivery of related goods or services, and any other terms, conditions, warranties or representations associated with such dealings, are solely between you and such advertiser. You agree that Duco Hosting shall not be responsible or liable for any loss or damage of any sort incurred as the result of any such dealings or as the result of the presence of such advertisers on the Service.

##23. Free hosting accounts

You acknowledge and agree that Duco Hosting may, at any time, choose to terminate a free hosting account without prior notice and/or reason. You also agree that a free hosting account may be allocated fewer server resources when a paid account requires them, paid hosting accounts will ALWAYS have priority over free hosting accounts for resource allocation.
You agree that Duco Hosting may, at any time, choose to reclaim a sub-domain granted for a free hosting account. In this case you will be notified in advance and be given the chance to request another sub-domain.
You agree to not use a free hosting account for (i)Commercial purposes; (ii)email hosting; or (iii)file storage.
Duco Hosting agrees to always allow owners of free hosting accounts to upgrade their account to a paid hosting account, when upgrading you will be charged an amount relative to the remaining days until the next due date of your account.

##24. Links

The Service may provide, or third parties may provide, links to other World Wide Web sites or resources. Because Duco Hosting has no control over such sites and resources, you acknowledge and agree that Duco Hosting is not responsible for the availability of such external sites or resources, and does not endorse and is not responsible or liable for any Content, advertising, products or other materials on or available from such sites or resources. You further acknowledge and agree that Duco Hosting shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such Content, goods or services available on or through any such site or resource.

##25. Duco Hosting's proprietary rights

You acknowledge and agree that the Service and any necessary software used in connection with the Service ("Software") contain proprietary and confidential information that is protected by applicable intellectual property and other laws. You further acknowledge and agree that Content contained in sponsor advertisements or information presented to you through the Service or by advertisers is protected by copyrights, trademarks, service marks, patents or other proprietary rights and laws. Except as expressly authorized by Duco Hosting or advertisers, you agree not to modify, rent, lease, loan, sell, distribute or create derivative works based on the Service or the Software, in whole or in part.

##26. Disclaimer of warranties

YOU EXPRESSLY UNDERSTAND AND AGREE THAT:

1. YOUR USE OF THE SERVICE IS AT YOUR SOLE RISK. THE SERVICE IS PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS. Duco Hosting AND ITS SUBSIDIARIES, AFFILIATES, OFFICERS, EMPLOYEES, AGENTS, PARTNERS AND LICENSORS EXPRESSLY DISCLAIM ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
2. Duco Hosting AND ITS SUBSIDIARIES, AFFILIATES, OFFICERS, EMPLOYEES, AGENTS, PARTNERS AND LICENSORS MAKE NO WARRANTY THAT (i) THE SERVICE WILL MEET YOUR REQUIREMENTS; (ii) THE SERVICE WILL BE UNINTERRUPTED, TIMELY, SECURE OR ERROR-FREE; (iii) THE RESULTS THAT MAY BE OBTAINED FROM THE USE OF THE SERVICE WILL BE ACCURATE OR RELIABLE; (iv) THE QUALITY OF ANY PRODUCTS, SERVICES, INFORMATION OR OTHER MATERIAL PURCHASED OR OBTAINED BY YOU THROUGH THE SERVICE WILL MEET YOUR EXPECTATIONS; AND (v) ANY ERRORS IN THE SOFTWARE WILL BE CORRECTED.
3. ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE SERVICE IS ACCESSED AT YOUR OWN DISCRETION AND RISK, AND YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH MATERIAL.
4. NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM Duco Hosting OR THROUGH OR FROM THE SERVICE SHALL CREATE ANY WARRANTY NOT EXPRESSLY STATED IN THE TOS.
5. A SMALL PERCENTAGE OF USERS MAY EXPERIENCE EPILEPTIC SEIZURES WHEN EXPOSED TO CERTAIN LIGHT PATTERNS OR BACKGROUNDS ON A COMPUTER SCREEN OR WHILE USING THE SERVICE. CERTAIN CONDITIONS MAY INDUCE PREVIOUSLY UNDETECTED EPILEPTIC SYMPTOMS EVEN IN USERS WHO HAVE NO HISTORY OF PRIOR SEIZURES OR EPILEPSY. IF YOU, OR ANYONE IN YOUR FAMILY, HAVE AN EPILEPTIC CONDITION, CONSULT YOUR PHYSICIAN PRIOR TO USING THE SERVICE. IMMEDIATELY DISCONTINUE USE OF THE SERVICE AND CONSULT YOUR PHYSICIAN IF YOU EXPERIENCE ANY OF THE FOLLOWING SYMPTOMS WHILE USING THE SERVICE: DIZZINESS, ALTERED VISION, EYE OR MUSCLE TWITCHES, LOSS OF AWARENESS, DISORIENTATION, ANY INVOLUNTARY MOVEMENT, OR CONVULSIONS.

##27. Limitation of Liability

YOU EXPRESSLY UNDERSTAND AND AGREE THAT Duco Hosting AND ITS SUBSIDIARIES, AFFILIATES, OFFICERS, EMPLOYEES, AGENTS, PARTNERS AND LICENSORS SHALL NOT BE LIABLE TO YOU FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES, INCLUDING, BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES (EVEN IF Duco Hosting HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES), RESULTING FROM: (i) THE USE OR THE INABILITY TO USE THE SERVICE; (ii) THE COST OF PROCUREMENT OF SUBSTITUTE GOODS AND SERVICES RESULTING FROM ANY GOODS, DATA, INFORMATION OR SERVICES PURCHASED OR OBTAINED OR MESSAGES RECEIVED OR TRANSACTIONS ENTERED INTO THROUGH OR FROM THE SERVICE; (iii) UNAUTHORIZED ACCESS TO OR ALTERATION OF YOUR TRANSMISSIONS OR DATA; (iv) STATEMENTS OR CONDUCT OF ANY THIRD PARTY ON THE SERVICE; OR (v) ANY OTHER MATTER RELATING TO THE SERVICE.

##28. Exclusions and Limitations

SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN WARRANTIES OR THE LIMITATION OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES. ACCORDINGLY, SOME OF THE ABOVE LIMITATIONS OF SECTION 26 MAY NOT APPLY TO YOU.

##29. No third-party beneficiaries

You agree that, except as otherwise expressly provided in this TOS, there shall be no third-party beneficiaries to this agreement.

##30. Notice

Duco Hosting may provide you with notices, including those regarding changes to the TOS, by email, regular mail or postings on the Service.

##31. Notice and Procedure for making claims of copyright or intellectual property infringement

Duco Hosting respects the intellectual property of others, and we ask our users to do the same. Duco Hosting may, in appropriate circumstances and at its discretion, disable and/or terminate the accounts of users who may be repeat infringers. If you believe that your work has been copied in a way that constitutes copyright infringement, or your intellectual property rights have been otherwise violated, please provide Duco Hosting 's Copyright Agent the following information:

an electronic or physical signature of the person authorized to act on behalf of the owner of the copyright or other intellectual property interest;

a description of the copyrighted work or other intellectual property that you claim has been infringed;
a description of where the material that you claim is infringing is located on the site;
your address, telephone number, and email address;
a statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law;
a statement by you, made under penalty of perjury, that the above information in your Notice is accurate and that you are the copyright or intellectual property owner or authorized to act on the copyright or intellectual property owner's behalf.
Duco Hosting 's Agent for Notice of claims of copyright or other intellectual property infringement can be reached as follows:

By mail:

> Duco Hosting
> Grassavanne 30
> 5658 EV  Eindhoven
> The Netherlands

By email: claims@ducohosting.com

##32. General Information

**Entire Agreement.**

The TOS constitutes the entire agreement between you and Duco Hosting and governs your use of the Service, superseding any prior agreements between you and Duco Hosting with respect to the Service. You also may be subject to additional terms and conditions that may apply when you use or purchase certain other Duco Hosting services, affiliate services, third-party content or third-party software.

**Choice of Law and Forum.**

The TOS and the relationship between you and Duco Hosting shall be governed by the laws of the Netherlands without regard to its conflict of law provisions. You and Duco Hosting agree to submit to the personal and exclusive jurisdiction of the courts located within the country of the Netherlands.

**Waiver and Severability of Terms.**

The failure of Duco Hosting to exercise or enforce any right or provision of the TOS shall not constitute a waiver of such right or provision. If any provision of the TOS is found by a court of competent jurisdiction to be invalid, the parties nevertheless agree that the court should endeavor to give effect to the parties' intentions as reflected in the provision, and the other provisions of the TOS remain in full force and effect.

**No Right of Survivorship and Non-Transferability.**

You agree that your Duco Hosting account is non-transferable and any rights to your Duco Hosting ID or contents within your account terminate upon your death. Upon receipt of a copy of a death certificate, your account may be terminated and all contents therein permanently deleted.

**Statute of Limitations.**

You agree that regardless of any statute or law to the contrary, any claim or cause of action arising out of or related to use of the Service or the TOS must be filed within one (1) year after such claim or cause of action arose or be forever barred.
The section titles in the TOS are for convenience only and have no legal or contractual effect.

#Acceptable use Policy

##Illegal use

Duco Hosting services may, under no circumstances, be used for any illegal activity. Services found to be used for any illegal activities will be immediately terminated without refund.

##Forgery and Impersonation

Adding, removing or modifying identifying network header information in an effort to deceive or mislead is prohibited. Attempting to impersonate any person or legal entity by using forged headers or other
identifying information is prohibited. Additionally, uploading content in an effort to acquire private information such as banking details, usernames or other such personal info ("Phishing") is strictly prohibited.
Any person found in violation of this rule will have their services immediately terminated without refunded and reported to appropriate authorities.


##Network disruptions and unfriendly activity

Use of the Duco Hosting service for any activity which affects the ability of other people or systems to use Duco Hosting Services or the Internet. This includes "Denial of Service" (DoS)
attacks against another network host or individual user. Interference with or disruption of other network users, services or equipment is prohibited. It is the Member's responsibility to
ensure that their network is configured in a secure manner. A Subscriber may not, through action or inaction, allow others to use their network for illegal or inappropriate actions.
A Subscriber may not permit their network, through action or inaction, to be configured in such a way that gives a third party the capability to use their network in an illegal or inappropriate manner.
Unauthorized entry and/or use of another company and/or individual's computer system will result in immediate account termination. Duco Hosting will not tolerate any subscriber attempting to access the
accounts of others, or penetrate security measures of other systems, whether or not the intrusion results in corruption or loss of data.
